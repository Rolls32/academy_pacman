﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PAC_Man.Pages
{
    public class GameModel : PageModel
    {
        public enum FieldBox
        {
            Wall = 0,
            Coin = 1,
            PacMan = 2,
            Tunnel = 3,
            Tunnel1 = 4,
            Empty = 5
        }
        
        public FieldBox[,] mapArray { get; set; } = { 
        { FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Tunnel, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall}, //c = 19, r = 22    tunnel: x0, y10 tunnel1: x18, y10: 
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin/*enter*/, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.PacMan, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall, FieldBox.Coin, FieldBox.Coin, FieldBox.Coin, FieldBox.Wall},
        { FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Tunnel1, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall, FieldBox.Wall}
        };

        public void OnGet()
        {
        }
    }
}