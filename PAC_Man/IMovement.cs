﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static PAC_Man.Pages.GameModel;

namespace PAC_Man
{
    public interface IMovement
    {
        FieldBox[,] Move(int direction);
    }
}
