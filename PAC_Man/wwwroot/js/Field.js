﻿var pacMan;
function startGame() {
    gameField.start();
    pacMan = new pacman(32, 32, "../images/Pacman.gif", 200, 200, "image");
}

var gameField = {
    canvas: document.getElementById("canvField"),
    start: function () {
        this.canvas.width = 600;
        this.canvas.height = 400;
        this.context = this.canvas.getContext("2d");
    }
}

function pacman(width, height, color, x, y, type) {
    this.type = type
    if (type == "image") {
        this.image = new Image();
        this.image.src = color;
    }

    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;
    this.x = x;
    this.y = y;
    this.update = function () {
        ctx = gameField.context;
        if (type == "image") {
            ctx.drawImage(this.image,
                this.x,
                this.y,
                this.width, this.height);
        } else {
            ctx.fillStyle = color;
            ctx.fillRect(this.x, this.y, this.width, this.height);
        }
        this.newPos = function () {
            this.x += this.speedX;
            this.y += this.speedY;
        }
    }
}