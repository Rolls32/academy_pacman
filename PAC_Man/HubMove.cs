﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PAC_Man.Pages;
using Microsoft.AspNetCore.SignalR;

namespace PAC_Man
{
    //public enum Direction
    //{
    //    Left,
    //    Up,
    //    Right,
    //    Down
    //}
    
    public class HubMove : Hub
    {
        private IMovement _movement;

        public HubMove(IMovement movement)
        {
            _movement = movement;
        }
        
        public async Task hMove(int direction)
        {
            await this.Clients.Caller.SendAsync("Movement", _movement.Move(direction));
        }
    }
}
