﻿using PAC_Man.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAC_Man
{
    public class Movement : GameModel, IMovement
    {
        public Tuple<int, int> GetPacMan()
        {
            for (int pacManX = 0; pacManX < mapArray.GetLength(0); pacManX++)
            {
                for (int pacManY = 0; pacManY < mapArray.GetLength(1); pacManY++)
                {
                    if (mapArray[pacManX, pacManY].Equals(FieldBox.PacMan))
                    {
                        return new Tuple<int, int>(pacManX, pacManY);
                    }
                }
            }
            return new Tuple<int, int>(0, 0);
        }

        public FieldBox[,] MoveTo(int x, int y)
        {
            var coord = GetPacMan();
            int pacManX = coord.Item1;
            int pacManY = coord.Item2;

            if (mapArray[pacManX + x, pacManY + y].Equals(FieldBox.Wall))
            {
                return mapArray;
            }
            else
            {
                if (mapArray[pacManX + x, pacManY + y].Equals(FieldBox.Tunnel))
                {
                    mapArray[17, 10] = FieldBox.PacMan;
                    mapArray[pacManX, pacManY] = FieldBox.Empty;
                    mapArray[pacManX + x, pacManY + y] = FieldBox.Tunnel;
                    return mapArray;
                }
                else if (mapArray[pacManX + x, pacManY + y].Equals(FieldBox.Tunnel1))
                {
                    mapArray[1, 10] = FieldBox.PacMan;
                    mapArray[pacManX, pacManY] = FieldBox.Empty;
                    mapArray[pacManX + x, pacManY + y] = FieldBox.Tunnel1;
                    return mapArray;
                }
                else if (mapArray[pacManX + x, pacManY + y].Equals(FieldBox.Coin))
                {
                    mapArray[pacManX + x, pacManY + y] = FieldBox.PacMan;
                    mapArray[pacManX, pacManY] = FieldBox.Empty;
                    return mapArray;
                }
                else if (mapArray[pacManX + x, pacManY + y].Equals(FieldBox.Empty))
                {
                    mapArray[pacManX + x, pacManY + y] = FieldBox.PacMan;
                    mapArray[pacManX, pacManY] = FieldBox.Empty;
                    return mapArray;
                }
                return mapArray;
            }
        }

        public FieldBox[,] Move(int direction)
        {
            if (direction == 65) //left
            {
                return MoveTo(-1, 0);
            }
            else if (direction == 87) //top
            {
                return MoveTo(0, -1);
            }
            else if (direction == 68) //right
            {
                return MoveTo(1, 0);
            }
            else if (direction == 83) //bottom
            {
                return MoveTo(0, 1);
            }
            else
            {
                return mapArray;
            }
        }
    }
}